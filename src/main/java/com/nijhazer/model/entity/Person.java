package com.nijhazer.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="person")
public class Person implements Serializable {
	private static final long serialVersionUID = 8399590575491470180L;

	private long id;
    private String firstName;
    private String lastName;
    
    @Id
    @Column(name="id")
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Column(name="first_name")
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="last_name")
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Transient
	public String getFullName() {
		return new StringBuffer()
		        .append(this.getLastName())
				.append(", ")
				.append(this.getFirstName())
				.toString();
	}
	
	@Override
	public String toString() {
		return new StringBuffer()
		        .append("[Person] ")
			    .append(this.getFullName())
				.toString();
	}
}
