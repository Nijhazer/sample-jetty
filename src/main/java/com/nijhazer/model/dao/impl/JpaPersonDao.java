package com.nijhazer.model.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.nijhazer.model.dao.PersonDao;
import com.nijhazer.model.entity.Person;

public class JpaPersonDao implements PersonDao {
	private EntityManagerFactory entityManagerFactory;
	
	public Person getPersonById(long id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Person person = entityManager.find(Person.class, id);
		entityManager.close();
		return person;
	}

	public List<Person> getPeopleByFirstName(String firstName) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<Person> people = (List<Person>) entityManager.createQuery("SELECT a FROM Person a WHERE a.firstName LIKE :firstName").setParameter("firstName", firstName).getResultList();
		entityManager.close();
		return people;
	}

	public List<Person> getPeopleByLastName(String lastName) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<Person> people = (List<Person>) entityManager.createQuery("SELECT a FROM Person a WHERE a.lastName LIKE :lastName").setParameter("lastName", lastName).getResultList();
		entityManager.close();
		return people;
	}

	public void addPerson(Person person) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.persist(person);
		entityManager.close();
	}

	public void updatePerson(Person person) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Person personToUpdate = entityManager.find(Person.class, person.getId());
        personToUpdate.setFirstName(person.getFirstName());
        personToUpdate.setLastName(person.getLastName());
        entityManager.merge(personToUpdate);
        entityManager.flush();
        entityManager.close();
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
}
