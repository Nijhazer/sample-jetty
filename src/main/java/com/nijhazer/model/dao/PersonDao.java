package com.nijhazer.model.dao;

import java.util.List;

import com.nijhazer.model.entity.Person;

public interface PersonDao {
    public Person getPersonById(long id);
    public List<Person> getPeopleByFirstName(String firstName);
    public List<Person> getPeopleByLastName(String lastName);
    public void addPerson(Person person);
    public void updatePerson(Person person);
}
